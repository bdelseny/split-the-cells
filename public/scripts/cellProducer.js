import { Calculation } from "./calculation.js";

/**
 * Cell producer object
 * @author BDY
 */
export function CellProducer() {

    let self = this; // Cell producer object context
    let calculation = new Calculation();

    /**
    * Factory for cells production
    * @author BDY
    */
    this.produceCells = function() {
        switch(this.name) {
            case "cellProducerButton":
                document.querySelector('#NumberOfCells').value = self.produceCellsOnManualClick(document.querySelector('#NumberOfCells').value);
                break;
            default:
                console.log("No cells to add");
        }
    };

    /**
    * Produce the cells on manual click
    * @param {number} initialValue the number of cells before function call
    * @return {number} the number of cells after production
    * @author BDY
    */
    this.produceCellsOnManualClick = function(initialValue) {
        return +initialValue + calculation.calculateCellsToAdd("manualProduction");
    }
}