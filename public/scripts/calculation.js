

export function Calculation() {
    let self = this;

    /**
     * Factory to calculate cells to add
     * @param {string} caller the function caller
     * @return {number} the calculated value
     * @author BDY
     */
    this.calculateCellsToAdd = function (caller) {
        let value = 0;
        switch (caller) {
            case "manualProduction":
                value = self.calculateCellsForManualClick();
                break;
            default:
                value = 0;
        }
        return value
    };

    /**
     * Calculate cells to add on manual click
     * @return {number} the calculated value
     */
    this.calculateCellsForManualClick = function () {
        return 1;
    };
};